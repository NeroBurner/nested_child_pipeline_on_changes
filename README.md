# nested child pipeline with rules: changes

This repository is to show, that `rules: changes` can't be used for nested child pipelines.
Those pipelines don't have a `push` event associated to them.
